package com.hexagone.commande.security;

import org.keycloak.adapters.springboot.KeycloakSpringBootConfigResolver;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

/**
 * | @Configuration et @EnableWebSecurity vont permettre de définir que cette classe comme la classe de configuration pour spring et va activer spring sécurity pour les aplications web
 * | @ComponentScan(basePackageClasses = KeycloakSecurityComponents.class) va demander à spring de scanner les packges au niveau de la classe  "KeycloakSecurityComponents".
 * Le scanner permettra à spring de se préconfigurer pour keycloack
 *
 * Création de la classe securityConfig
 **/
@Configuration
@EnableWebSecurity
@ComponentScan(basePackageClasses = KeycloakSecurityComponents.class)
class SecurityConfig extends KeycloakWebSecurityConfigurerAdapter {

    /** configureGlobal indique à spring d'utiliser keycloak comme fournisseur d'authentification. **/
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(new SimpleAuthorityMapper());
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    /**KeycloakConfigResolver permet à keycloak de lire le fichier application.yml pour trouver les paramètres serveurs.Si cette méthode n'est pas présnete, les paramètre de application.yaml ne seront pas utilisés.**/
    @Bean
    public KeycloakSpringBootConfigResolver KeycloakConfigResolver() {
        return new KeycloakSpringBootConfigResolver();
    }

    /**sessionAuthenticationStrategy permet de communiquer avec spring pour lui dire d'utiliser la stratégie de session utilisateur par defaut.**/
    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
    }

    /**
     * configure(HttpSecurity http) va permettre de gérer les droits d'accès aux ressources web
     * .authorizeRequests() va démarrer un groupe de configuration d'authentification des requêtes web
     * .antMatchers() permet de définir sur quelle url ces règles d'autorisation doivent être actives
     * .hasAnyRole() permet aux utilisateur d'avoir accès à l'ensemble des URLs
     * .and() permet de démarrer un nouveau groupe de configuration
     * .csrf().disable() permet de désactiver la protection csrf
     * Configuration de la sécurité pour les différents rôles : Client, Gouvernement, Livreur, Administrateur **/
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);


        http

                .authorizeRequests()
                .antMatchers("/commandes*")
                .hasAnyRole("Client","Administrateur","Livreur", "Gouvernement")
                .and()
                .csrf()
                .disable();
    }
}