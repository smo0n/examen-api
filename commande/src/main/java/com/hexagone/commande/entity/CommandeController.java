package com.hexagone.commande.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


/**
 * Lorsqu'une classe possède un @RestController, elle va être géré pas spring
 * Cela va permettre que toutes les valeurs de retour de l'ensemble des méthodes puissent être sérialisés dans une réponse HTTP.
 * Création de la classe CommandeController **/
@RestController
public class CommandeController {

    /**
     * Le type Repository va permettre d'accèder à la base de donnée.
     * Chaque objet qui sont @Autowired pourront utiliser spring
     * Déclaration de ma valeur commandeRepository **/
    @Autowired
    CommandeRepository commandeRepository;

    /**Déclaration de ma valeur ligneCommandeRepository **/
    @Autowired
    LigneCommandeRepository ligneCommandeRepository;




    /**
     * Le @PostMapping va permettre de mapper une méthode et l'ensemble des ses paramètres à une URL HTTP.
     * Le Lien qui le suit va être l'URL à mapper
     * Lors qu'un URL est dynamique, il est possibe de mettre une variable associé à un parmètre de méthode.
     * Pour faire ça, je dois mettre un nom entre {}
     *
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring Commande.
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers commandes**/
    @PostMapping("/commandes")
    public Commande postCommande(@RequestBody Commande commande ){
        return commandeRepository.save(commande);
    }


    /** Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de commandes.**/
    @GetMapping("/commandes")
    public Iterable<Commande> getCommande(){
        return commandeRepository.findAll();
    }


    /**
     *  Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de commandes avec un id précis**/
    @GetMapping("/commandes/{id}")
    public Commande getCommandeById(@PathVariable Long id){
        return commandeRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    /** Méthode DeleteMapping qui va permettre de mapper des requêtes HTTP Delete qui viennent de commandes en fonction de l'id**/
    @DeleteMapping("/commandes/{id}")
    public void deleteCommande(@PathVariable Long id){
        commandeRepository.deleteById(id);
    }

    /** Méthode PutMapping qui va permettre de mapper des requêtes HTTP Put qui viennent de commandes/update en fonction de l'id
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring Commande.**/
    @PutMapping("/commandes/update/{id}")
    public Commande updateCommande(@PathVariable long id, @RequestBody Commande commande){
        Commande updatedcommande = commandeRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        updatedcommande=commande;
        commandeRepository.save(updatedcommande);
        return updatedcommande;
    }

    /**
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring LigneCommande.
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers addLigneCommande**/
    @PostMapping("/addLigneCommande")
    public void postLigneCommande(@RequestBody LigneCommande ligneCommande){
        ligneCommandeRepository.save(ligneCommande);
    }

    /** Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de addLigneCommande.**/
    @GetMapping(value = "/addLigneCommande", produces = APPLICATION_JSON_VALUE)
    public Iterable<LigneCommande> getLigneCommande(){
        return ligneCommandeRepository.findAll();
    }

    /**
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring Commande.
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers addCommande**/
    @PostMapping("/addCommande")
    public Commande postAddCommande(@RequestBody Commande commande) {
        return commandeRepository.save(commande);

    }
    /** Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de addCommande.**/
    @GetMapping("/addCommande")
    public Iterable<Commande> getAddCommande(){
        return commandeRepository.findAll();
    }


}
