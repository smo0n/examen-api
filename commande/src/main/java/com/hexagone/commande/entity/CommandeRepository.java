package com.hexagone.commande.entity;

import org.springframework.data.repository.CrudRepository;


/**
 * Une interface qui implement CrudRepository est injectable (@Autowired) par Spring.
 * Elle va permettre de mettre à disposition l'ensemble des méthodes pour faire des requête à la base de données
 */

public interface CommandeRepository extends CrudRepository< Commande,Long > {
}
