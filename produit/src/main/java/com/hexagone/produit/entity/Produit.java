package com.hexagone.produit.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * Une entity est une classe gérée par Spring qui va représenter une table en base de données.
 * Elle aura obligatoirement, une clé primaire défini par @Id.
 * Chaque attribut de ma classe représenteras une colonne de la table.
 * Le @generetedValue va permettre à spring de choisir la stratégie de génération d'attributs.
 * Création de la classe Produit **/
@Entity
public class Produit {

    /** Ici je vais créer les attributs de ma base de donnée : id, name, duree, instruction **/
    @Id
    @GeneratedValue

    public long id;
    public String name;
    public String duree;
    public String instruction;

    /** Méthode get pour ma valeur instruction **/
    public String getInstruction() {
        return instruction;
    }

    /** Méthode set pour ma valeur instruction **/
    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    /** Méthode get pour ma valeur id **/
    public long getId() {
        return id;
    }

    /** Méthode set pour ma valeur id **/
    public void setId(long id) {
        this.id = id;
    }

    /** Méthode get pour ma valeur name **/
    public String getName() {
        return name;
    }

    /** Méthode set pour ma valeur name **/
    public void setName(String name) {
        this.name = name;
    }

    /** Méthode get pour ma valeur duree **/
    public String getDuree() {
        return duree;
    }

    /** Méthode set pour ma valeur duree **/
    public void setDuree(String duree) {
        this.duree = duree;
    }
}
