package com.hexagone.produit.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

/**
 * Lorsqu'une classe possède un @RestController, elle va être géré pas spring
 * Cela va permettre que toutes les valeurs de retour de l'ensemble des méthodes puissent être sérialisés dans une réponse HTTP.
 * Création de la classe ProduitController **/
@RestController
public class ProduitController {

    /**
     * Le type Repository va permettre d'accèder à la base de donnée.
     * Chaque objet qui sont @Autowired pourront utiliser spring
     * Déclaration de ma valeur produitRepository **/
    @Autowired
    ProduitRepository produitRepository;

    /**
     * Le @PostMapping va permettre de mapper une méthode et l'ensemble des ses paramètres à une URL HTTP.
     * Le Lien qui le suit va être l'URL à mapper
     * Lors qu'un URL est dynamique, il est possibe de mettre une variable associé à un parmètre de méthode.
     * Pour faire ça, je dois mettre un nom entre {}
     *
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring Produit.
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers produits**/

    @PostMapping("/produits")
    public Produit postProduit(@RequestBody Produit produit) {
        return produitRepository.save(produit);

    }

    /** Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de produits.**/
    @GetMapping("/produits")
    public Iterable<Produit> getProduit(){
        return produitRepository.findAll();
    }

    /**Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de produits avec un id précis**/
    @GetMapping("/produits/{id}")
    public Produit getProduitById(@PathVariable Long id){
        return produitRepository.findById(id).orElseThrow(EntityNotFoundException::new);
    }

    /** Méthode DeleteMapping qui va permettre de mapper des requêtes HTTP Delete qui viennent de produits/delete en fonction de l'id**/
    @DeleteMapping("/produits/delete/{id}")
    public void deleteProduit(@PathVariable Long id){
        produitRepository.deleteById(id);
    }

    /** Méthode PutMapping qui va permettre de mapper des requêtes HTTP Put qui viennent de produits/update en fonction de l'id
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring Produit.**/
    @PutMapping("/produits/update/{id}")
    public Produit updateProduit(@PathVariable long id, @RequestBody Produit produit){
        Produit updatedproduit = produitRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        updatedproduit=produit;
        produitRepository.save(updatedproduit);
        return updatedproduit;
    }

    /**
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring Produit.
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers addProduit**/
    @PostMapping("/addProduit")
    public Produit postAddProduit(@RequestBody Produit produit) {
        return produitRepository.save(produit);

    }
    /** Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de addProduit.**/
    @GetMapping("/addProduit")
    public Iterable<Produit> getAddProduit(){
        return produitRepository.findAll();
    }

}
