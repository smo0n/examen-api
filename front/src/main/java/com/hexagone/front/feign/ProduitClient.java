package com.hexagone.front.feign;


import com.hexagone.front.entity.Produit;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/** Le paramètre @FeignClient va me permettre de relier mon front à mon micro service Produit
 * Création de la classe ProduitClient**/
@FeignClient(value = "Produit")
public interface ProduitClient {

    /** Je définis la valeur AUTH_TOKEN **/
    String AUTH_TOKEN= "Authorization";

    /** Pour chaque méthodes, je vais devoir récupérer le token pour l'authentification avec les le paramètre suivant:
     *| @RequestHeader(AUTH_TOKEN) String token
     *
     *
     *
     * Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de produits en utilisant le token **/
    @GetMapping("/produits")
    Iterable<Produit> getProduit(@RequestHeader(AUTH_TOKEN) String token);


    /**
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers produits en utilisant le token
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring Produit.
     * **/
    @PostMapping("/produits")
    Produit setProduit(@RequestHeader(AUTH_TOKEN) String token, @RequestBody Produit produit);


    /** Méthode DeleteMapping qui va permettre de mapper des requêtes HTTP Delete qui viennent de produits/delete en fonction de l'id en utilisant le token**/
    @DeleteMapping("/produits/delete/{id}")
    Produit deleteProduit(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id);


    /**
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers addProduit en utilisant le token
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring Produit.**/
    @PostMapping("/addProduit")
    Produit setAddProduit(@RequestHeader(AUTH_TOKEN) String token, @RequestBody Produit produit);


    /** Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de produits avec son id tout en utilisant le token **/
    @GetMapping("/produits/{id}")
    Optional<Produit> getId(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id);


    /**Méthode PutMapping qui va permettre de mapper des requêtes HTTP Put qui viennent de produits/update en fonction de l'id tout en utilisant le token
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring Produit.**/
    @PutMapping(path = "/produits/update/{id}")
    Produit updateProduit(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id, @RequestBody Produit produit);


}