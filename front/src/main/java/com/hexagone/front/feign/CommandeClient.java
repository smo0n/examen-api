package com.hexagone.front.feign;

import com.hexagone.front.entity.Commande;
import com.hexagone.front.entity.LigneCommande;
import com.hexagone.front.entity.Produit;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**Le paramètre @FeignClient va me permettre de relier mon front à mon micro service Commande
 * Création de la classe CommandeClient**/
@FeignClient(value = "Commande")
public interface CommandeClient {

    /** Je définis la valeur AUTH_TOKEN **/
    String AUTH_TOKEN= "Authorization";

    /** Pour chaque méthodes, je vais devoir récupérer le token pour l'authentification avec les le paramètre suivant:
     *| @RequestHeader(AUTH_TOKEN) String token
     *
     *
     *
     * Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de commandes en utilisant le token **/
    @GetMapping("/commandes")
    Iterable<Commande> getCommande(@RequestHeader(AUTH_TOKEN) String token);

    /**
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers commandes en utilisant le token
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring Commande.
     * **/
    @PostMapping("/commandes")
    Commande setCommande(@RequestHeader(AUTH_TOKEN) String token, @RequestBody Commande commande);


    /** Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de commandes avec son id tout en utilisant le token **/
    @GetMapping("/commandes/{id}")
    Optional<Commande> getId(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id);


    /**Méthode PutMapping qui va permettre de mapper des requêtes HTTP Put qui viennent de commandes/update en fonction de l'id tout en utilisant le token
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring Commande.**/
    @PutMapping(path = "/commandes/update/{id}")
    Commande updateCommande(@RequestHeader(AUTH_TOKEN) String token, @PathVariable("id") Long id, @RequestBody Commande commande);


    /**
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers addLigneCommande en utilisant le token
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring LigneCommande.**/
    @PostMapping("/addLigneCommande")
    LigneCommande postligneCommande(@RequestHeader(AUTH_TOKEN) String token, @RequestBody LigneCommande ligneCommande);


    /** Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de addLigneCommande tout en utilisant le token **/
    @GetMapping("/addLigneCommande")
    Iterable<LigneCommande> getLigneCommande(@RequestHeader(AUTH_TOKEN) String token);


    /**
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers addCommande en utilisant le token
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring Commande.**/
    @PostMapping("/addCommande")
    Commande setAddCommande(@RequestHeader(AUTH_TOKEN) String token, @RequestBody Commande commande);


}
