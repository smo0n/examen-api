package com.hexagone.front.home;

import com.hexagone.front.entity.Commande;
import com.hexagone.front.entity.LigneCommande;
import com.hexagone.front.entity.Produit;
import com.hexagone.front.feign.CommandeClient;
import com.hexagone.front.feign.ProduitClient;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Optional;

/**
 * L'annotation @Controller permet de jouer le rôle de contrôleur dans mon application.
 * L'annotation @Controller va permettre de faire de l'auto-detcetion des classes de composants dans les définitions de chemin de classe et d'enregistrement automatique des beans.
 * L'annotation @Controller sera capable de gérer plusieurs mapping de requêtes.
 * Création de la classe WebController **/
@Controller
class WebController {

    /**
     * Chaque objet qui sont @Autowired pourront utiliser spring
     * Déclaration de ma valeur produitClient **/
    @Autowired
    ProduitClient produitClient;

    /**
     * Chaque objet qui sont @Autowired pourront utiliser spring
     * Déclaration de ma valeur commandeClient **/
    @Autowired
    CommandeClient commandeClient;


    /**
     * Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de /
     * Cette méthode va me rediriger vers le home.html **/
    @GetMapping(path = "/")
    public String home() {
        return "home";
    }

    /**
     * Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de users
     * Cette méthode va me rediriger vers le users.html **/
    @GetMapping(path = "/users")
    public String customers(Principal principal, Model model) {
        model.addAttribute("username", principal.getName());
        return "users";
    }

    /** Pour l'ensemble des méthodes suivantes,je vais rajouter les lignes de commande suivantes:
     * KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
     * String token = context.getTokenString();
     * Ces commandes vont me permettre de récupérer le token pour la partie authentification.
     * Pour éviter les répétitions, je vous l'indique juste ici.
     *
     * Lors qu'un URL est dynamique, il est possibe de mettre une variable associé à un parmètre de méthode.
     * Pour faire ça, je dois mettre un nom entre {}
     *
     *
     * Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de produits
     * Cette méthode va me rediriger vers le produit.html tout en affichant l'ensemble de son contenu **/
    @GetMapping(path = "/produits")
    public String getProduit(HttpServletRequest request, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Iterable<Produit> produits = produitClient.getProduit("Bearer "+token);
        model.addAttribute("produits", produits);
        return "produit";
    }

    /**
     * Le @PostMapping va permettre de mapper une méthode et l'ensemble des ses paramètres à une URL HTTP.
     * Le Lien qui le suit va être l'URL à mapper
     *
     * Dans cette méthode, le corps de la requête va être récupéré pour la mettre dans l'@Entity spring Produit.
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers produits
     *
     * Cette méthode va envoyer des valeurs vers le produit.html **/
    @PostMapping("/produits")
    public String setProduit(@RequestBody Produit produit, HttpServletRequest request){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        produitClient.setProduit(token, produit);
        return "produit";
    }

    /**
     * Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de addProduit
     * Cette méthode va me rediriger vers  addProduit.html tout en affichant l'ensemble de son contenu**/
    @GetMapping("/addProduit")
    public String getAddProduit(HttpServletRequest request, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        model.addAttribute("produit", new Produit());
        return "addProduit";
    }

    /**
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers addProduits
     * Cette méthode va envoyer les valeurs de la page addProduit **/
    @PostMapping("/addProduit")
    public String postAddProduit(HttpServletRequest request, Model model, Produit produit){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Produit prod = produitClient.setAddProduit("Bearer "+token, produit);
        model.addAttribute("produit", prod);
        return "redirect:/";
    }

    /**
     * Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de produits/delete en fonction de son id
     * Cette méthode va me rediriger vers / tout en supprimant le produit avec l'id choisi **/
    @GetMapping("/produits/delete/{id}")
    public String deleteProduit(HttpServletRequest request, Model model, @PathVariable("id") Long id) {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Produit produit = produitClient.deleteProduit("Bearer "+token, id);
        return "redirect:/";
    }

    /**
     * Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de produits/update en fonction de son id
     * Cette méthode va me rediriger vers updateProduit.html. **/
    @GetMapping("/produits/update/{id}")
    public String updateProduit(HttpServletRequest request, Model model, @PathVariable("id") Long id){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Optional<Produit> produit = produitClient.getId("Bearer "+token,id);
        model.addAttribute("produit", produit.get());
        return "updateProduit";
    }

    /**
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers produits/update en fonction de son id
     * Cette méthode va envoyer les modifications effectuées sur le produit en question**/
    @PostMapping("/produits/update/{id}")
    public String updateProduit(HttpServletRequest request, Model model, @PathVariable("id") Long id, Produit produit){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Produit updateproduit = produitClient.updateProduit("Bearer "+token, id, produit);
        model.addAttribute("produit", updateproduit);
        return "redirect:/";
    }

    /**
     * Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de commandes
     * Cette méthode va me rediriger vers  commande.html tout en affichant l'ensemble de son contenu**/
    @GetMapping(path = "/commandes")
    public String getCommande(HttpServletRequest request, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Iterable<Commande> commandes = commandeClient.getCommande("Bearer "+token);
        model.addAttribute("commandes", commandes);
        return "commande";
    }

    /**
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers commandes
     * Cette méthode va envoyer les valeurs vers commandes */
    @PostMapping("/commandes")
    public String setCommande(@RequestBody Commande commande, HttpServletRequest request){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        commandeClient.setCommande(token, commande);
        return "commande";
    }

    /**
     * Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de commandes/update en fonction de son id
     * Cette méthode va me rediriger vers updateCommande.html. **/
    @GetMapping("/commandes/update/{id}")
    public String updateCommande(HttpServletRequest request, Model model, @PathVariable("id") Long id){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Optional<Commande> commande = commandeClient.getId("Bearer "+token,id);
        model.addAttribute("commande", commande.get());
        return "updateCommande";
    }

    /**
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers commandes/update en fonction de son id
     * Cette méthode va envoyer les modifications effectuées sur la commande en question**/
    @PostMapping("/commandes/update/{id}")
    public String updateCommande(HttpServletRequest request, Model model, @PathVariable("id") Long id, Commande commande){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Commande updatecommande = commandeClient.updateCommande("Bearer "+token, id, commande);
        model.addAttribute("commande", updatecommande);
        return "redirect:/";
    }


    /**
     * Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de addLigneCommande en fonction de son id
     * Cette méthode va me rediriger vers addLigneCommande.html. **/
    @GetMapping(path = "/addLigneCommande/{id}")
    public String getAddLigneCommande(HttpServletRequest request, Model model, @PathVariable("id") Long id){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        model.addAttribute("ligneCommande", new LigneCommande());
        Optional<Produit> produit = produitClient.getId("Bearer "+token, id);
        model.addAttribute("produit", produit.get());
        return "addLigneCommande";
    }

    /**
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers addLigneCommande en fonction de son id
     * Cette méthode va envoyer les valeurs du fichier addLigneCommande**/
    @PostMapping(path = "/addLigneCommande")
    public String postLigneCommande(HttpServletRequest request, Model model, LigneCommande ligneCommande){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        LigneCommande ligneCom = commandeClient.postligneCommande("Bearer "+token, ligneCommande);
        model.addAttribute("ligneCommande", ligneCom);
        return "redirect:/";
    }

    /**
     * Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de addLigneCommande
     * Cette méthode va me rediriger vers panier.html. **/
    @GetMapping(path = "/addLigneCommande")
    public String getLigneCommande(HttpServletRequest request, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Iterable<LigneCommande> ligneCommandes = commandeClient.getLigneCommande("Bearer "+token);
        model.addAttribute("ligneCommande", ligneCommandes);
        return "panier";
    }


    /**
     * Méthode GetMapping qui va permettre de mapper des requêtes HTTP Get qui viennent de addProduit
     * Cette méthode va me rediriger vers  addProduit.html tout en affichant l'ensemble de son contenu**/
    @GetMapping("/addCommande")
    public String getAddCommande(HttpServletRequest request, Model model){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        model.addAttribute("commande", new Commande());
        return "addCommande";
    }

    /**
     * Méthode PostMapping qui va permettre de mapper des requêtes Post vers addCommande
     * Cette méthode va envoyer les valeurs de la page addCommande **/
    @PostMapping("/addCommande")
    public String postAddCommande(HttpServletRequest request, Model model, Commande commande){
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        String token = context.getTokenString();
        Commande com = commandeClient.setAddCommande("Bearer "+token, commande);
        model.addAttribute("commande", com);
        return "redirect:/";
    }

}
