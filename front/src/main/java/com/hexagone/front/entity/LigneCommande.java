package com.hexagone.front.entity;

import javax.persistence.*;

/**
 * Une entity est une classe gérée par Spring qui va représenter une table en base de données.
 * Elle aura obligatoirement, une clé primaire défini par @Id.
 * Chaque attribut de ma classe représenteras une colonne de la table.
 * Le @generetedValue va permettre à spring de choisir la stratégie de génération d'attributs.
 * Création de la classe LigneCommande **/
@Entity
public class LigneCommande {

    /** Ici je vais créer les attributs de ma base de donnée : id, quantite, produit_id **/
    @Id
    @GeneratedValue
    public long id;
    public int quantite;
    public long produit_id;

    /** Méthode get pour ma valeur id**/
    public long getId() {
        return id;
    }

    /** Méthode set pour ma valeur id**/
    public void setId(long id) {
        this.id = id;
    }

    /** Méthode get pour ma valeur quantite**/
    public int getQuantite() {
        return quantite;
    }

    /** Méthode set pour ma valeur quantite**/
    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    /** Méthode get pour ma valeur produit_id**/
    public long getProduit_id() {
        return produit_id;
    }

    /** Méthode set pour ma valeur produit_id**/
    public void setProduit_id(long produit_id) {
        this.produit_id = produit_id;
    }
}
