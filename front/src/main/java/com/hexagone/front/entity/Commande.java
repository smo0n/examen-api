package com.hexagone.front.entity;

import javax.persistence.*;

/**
 * Une entity est une classe gérée par Spring qui va représenter une table en base de données.
 * Elle aura obligatoirement, une clé primaire défini par @Id.
 * Chaque attribut de ma classe représenteras une colonne de la table.
 * Le @generetedValue va permettre à spring de choisir la stratégie de génération d'attributs.
 * Création de la classe Commande **/
@Entity
public class Commande {

    /** Ici je vais créer les attributs de ma base de donnée : id, dateCommande, adresseLivraison... **/
    @Id
    @GeneratedValue
    public long id;
    public String dateCommande;
    public  String adresseLivraison;
    public String livreur;
    public String status;
    public int quantite;
    public String produit_id;



    /** Méthode get pour ma valeur id **/
    public long getId() {
        return id;
    }

    /** Méthode set pour ma valeur id **/
    public void setId(long id) {
        this.id = id;
    }

    /** Méthode get pour ma valeur dateCommande **/
    public String getDateCommande() {
        return dateCommande;
    }

    /** Méthode set pour ma valeur dateCommande **/
    public void setDateCommande(String dateCommande) {
        this.dateCommande = dateCommande;
    }

    /** Méthode get pour ma valeur adresseLivraison **/
    public String getAdresseLivraison() {
        return adresseLivraison;
    }

    /** Méthode set pour ma valeur adresseLivraison **/
    public void setAdresseLivraison(String adresseLivraison) {
        this.adresseLivraison = adresseLivraison;
    }

    /** Méthode get pour ma valeur livreur **/
    public String getLivreur() {
        return livreur;
    }

    /** Méthode set pour ma valeur livreur **/
    public void setLivreur(String livreur) {
        this.livreur = livreur;
    }

    /** Méthode get pour ma valeur status **/
    public String getStatus() {
        return status;
    }

    /** Méthode set pour ma valeur status **/
    public void setStatus(String status) {
        this.status = status;
    }


    /** Méthode get pour ma valeur quantite **/
    public int getQuantite() {
        return quantite;
    }

    /** Méthode set pour ma valeur quantite **/
    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    /** Méthode get pour ma valeur produit_id **/
    public String getProduit_id() {
        return produit_id;
    }

    /** Méthode set pour ma valeur produit_id **/
    public void setProduit_id(String produit_id) {
        this.produit_id = produit_id;
    }
}
